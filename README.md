# Hermitcraft Video Transcripts

|  |  |
| --- | --- |
| Curent Project Maintainer | Andrei Jiroh Halili @ajhalili2006 |
| Code/Website License      | Code under MIT, website content under CC BY-SA 4.0, excluding images and embedded content |
| maintaenance Status       | Experimental |
| Resulting Website Build   | <https://community-lores.gitlab.io/hermitcrat/transcripts> or <https://hermitcraft.transcripts.community-lores.gq> |
| Tech Stack Used In Repo   | Mkdocs with Material theme for website, GitLab for repo hosting, CI and static page hosting, Gitpod for dev environments |

This is the repository where you'll find unofficial video transcripts for different Hermitcraft series, including:

* the usual vanila and modded survival seasons
* Last Life and UHC
* and community-run HC-related channels and series vetted by maintainers, such as Hermitcraft Recap

While the transcripts are usually written in mixture of American and British English, we hope to also accept translations and
localized versions of these transcripts in the future.

## Transcript Status Tracker

We track the development of the transcripts below and in [this GitLab issue][roadmap-issue], with [an separate issue][roadmap-pod-hcrecap] for the Hermitcraft Recap in its unofficial podcast form.

|       Series Name               |     Type     | Season | Language[^1] | Path to directory within `src` |        Development Status      |
| ------------------------------- | ------------ | ------ | ------------ | ------------------------------ | ------------------------------ |
| Hermitcraft - Vanila            |     Main     |   S8   |     EN       | `hermitcraft/vanila/s8`        |      TODO / HELP WANTED        |
|                                 |              |   S9   |     EN       | `hermitcraft/vanila/s9`        |      TODO / HELP WANTED        |
| Last Life, formerly 3rd Life    | Experimental |   S1   |     EN       | `last-life/s1`                 |      TODO / HELP WANTED        |
|                                 |              |   S2   |     EN       | `last-life/s2`                 | WORK IN PROGRESS / HELP WANTED |
| Hermitcraft Recap               |  Community   |   S9   |     EN       | `community/hc-recap/s9`        | WORK IN PROGRESS / HELP WANTED |
| Simplified History by <TODO>    |  Community   |   N/A  |     EN       | N/A                            |      TODO / HELP WANTED        |

[^1]: Consult https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes for the list and find your language's code before editing here when submiting new translations.

[roadmap-issue]: https://gitlab.com/community-lores/hermitcrat/transcripts/issues/todo
[roadmap-pod-hcrecap]: https://gitlab.com/community-lores/hermitcrat/transcripts/issues/2
