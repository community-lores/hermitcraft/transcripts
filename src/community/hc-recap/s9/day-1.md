---
title: Hermitcraft Recap - Season 9 Week 3
---

??? warning "Page is work in progress"
    We're currently formatting stuff from the contents of <https://gitlab.com/community-lores/hermitcraft/transcripts/-/snippets/1234abc> to be nice with Mkdocs.

## Metadata

| Periods Covered | |
| --- | --- |
| YouTube Upload Date - UTC | TBD |
| Reason.fm Podcast Episode Publication Date - UTC | TBD |
| Summary | This week on Hermitcraft, ... |


## Where to watch/listen?

Only the YouTube link is official, the rest are unofficial channels in audio podcast form maintained by Andrei Jiroh.

* **YouTube**: https://youtu.be/
* **Reason Podcasts**: https://reason.fm/
* **Spotify**: https://open.spotify.com/episode/
* **Apple Podcasts**: TODO

## Episode Transcript

!!! note "Transcripts are provided to you from YouTube captions written by Lyarrah."
    We're format what we copied from the transcripts to its Markdown form behind the scenes. Come back later for updates.

**Pixlriffs**: This week on Hermitcraft…

