---
title: Hermitcraft Recap - Season 9 Week 3
---

??? warning "Page is work in progress"
    We're currently formatting stuff from the contents of <https://gitlab.com/community-lores/hermitcraft/transcripts/-/snippets/2283841> to be nice with Mkdocs.

## Metadata

| Periods Covered | |
| --- | --- |
| YouTube Upload Date - UTC | |
| Reason.fm Podcast Episode Publication Date - UTC | |
| Summary | |


## Where to watch/listen?

Only the YouTube link is official, the rest are unofficial channels in audio podcast form maintained by Andrei Jiroh.

* **YouTube**: https://www.youtube.com/watch?v=wVPRbkkhXdE
* **Reason Podcasts**: https://reason.fm/podcast/hermitcraft-recap-a-show-by-fans-for-fans-unofficial/hermitcraft-recap-season-9-week-3
* **Spotify**: https://open.spotify.com/episode/2FJbPdilCZMFKTsedJ7gAU?si=fl8vx1i0TnaYwjatDYGhOw
* **Apple Podcasts**: TODO

## Episode Transcript

!!! note "Transcripts are provided to you from YouTube captions written by Lyarrah."
    We're format what we copied from the transcripts to its Markdown form behind the scenes. Come back later for updates.

**Pixlriffs**: This week on Hermitcraft…

**FalseSymmetry** That ti- I'll be honest, that timing. Did you see  that timing? He ain't ready for me though, chat.

**Pixlriffs**: Welcome to the Hermitcraft Recap! My name is Pixlriffs, our writer is ZloyXP, captions on this video were provided by Lyarrah - and as the unofficial Recap Team, we get very excited when people team up. Because teamups are where shenanigans happen, and this week sees a bunch of Hermits adopting the buddy system to get stuff done. Which is especially bad news if your name is The Wither. But it’s good news if you have an Ocean Temple to dry out, a bunch  of black dye to harvest, or the need to acquire some wings from the dark and distant void. So in the spirit of these dream teams forming on the server, grab a friend, a family member, or the nearest household pet, and let’s take look at all the events and mishaps that occurred on the Hermitcraft Server this week!

**Pixlriffs**: Starting with that ocean temple we mentioned, and the holographic person at the center of it: GeminiTay.

**GeminiTay**: This is perfect! Look at us!

**Stress**: [laughing] I can't! 

**GeminiTay**: Ahh, true! Sorry! My bad!

**Stress**: 🤣

**Pixlriffs**: Once her original shop brings in any amount of money, Gem zeroes in on the next opportunity for profit: a prismarine shop. Naturally, you’d first need a prismarine farm, and it doesn’t help that most designs demand you find, conquer, and drain an ocean monument. What does help is a bunch of Hermitcraft  members, coming to her aid at another Hermits Helping Hermits event. This is also where  Docm comes in with his infinite sand stash, but only in exchange for unlimited  access to the future prismarine reserves. Also Stressmonster joins in as a more of a permanent business partner. So of course when it’s time to place  a glass outer shell on the perimeter, they chose pink one. Truly, outrageous.

**GeminiTay**: Oh. My. Gosh. Okay... haha. This is gonna be good. 

**Docm77**: [chuckles] I'd say you can have the sand for free, if I get free access to the farm later on and  can uh make myself prismarine blocks and stuff.

**Pixlriffs**: Joe Hills is just one of the Helping Hermits, and while he places a fair amount of the  sand and clears a good chunk of the seabed, his other role seems to be as a peacemaker for iJevin and ZombieCleo, whose playful game of murder-each-other is kinda getting  in the way of sponging all the water away.

**iJevin**: Cleo, on a scale of 1 to Salty, what are you right now?

**ZombieCleo**: Oh, uh, I am currently at, "all the salt in the ocean-

**iJevin**: [laughs] Oh my god.

**ZombieCleo**: -aaand you're going to suffer."

**JoeHills**: Yeah, Jev, maybe just let it drop-

**ZombieCleo**: Is where I am.

**JoeHills**: Why don't we- why don't we let GeminiTay sing the travelling song? Since this is *her* Hermits Helping Hermits, she gets to pick a travelling song! [Cleo and Jevin laugh]

**Pixlriffs**: In exchange for curbing the violence between the two of them, they persuade Joe not to do violence to his own bank account  by attempting to buy a Scottish castle. Joe, there’s a reason you’ve built two castles in the last three Hermitcraft seasons. It’s because they’re much more manageable as a digital offering.

**JoeHills**: Grian says, "The reason is: you'd be  obliged to restore it to the way it was, using stone masonry and as close to the  original stone as possible." Ohhh... Yeah yeah, that would-

**ZombieCleo**: Yeah, 'cause it would be publicly listed.

**JoeHills**: Yeah, that makes sense. 😞

**GeminiTay**: Sounds like a pain in the butt! 😊

**Pixlriffs**: As the last piece of sand falls on the freshly drained temple perimeter, Gem and Stressmonster can pat themselves on the back for a job well done. Which they couldn’t before, because their backs were invisible. Stress’s next team-up arrives in the form of Iskall, and the pair go wither skull hunting in the nearest Nether fortress. And if beheading all those skeletons wasn’t enough, Iskall loses his head in the process.

**Iskall, playing Overside in the background**: Huh? Iskall! Iskall...Man!

**Pixlriffs**: After cheesing the Wither for a beacon, Stress sets up a glass shop partnership with FalseSymmetry, and discovering the EnchanTEA has sold pretty well, returns to her forest base to  log all her trading villagers. And a giant log it turns out to be, with mushrooms growing from it and everything. Good thing these villagers aren’t spawning loads of cats, or it might end up as a catalog.
Iskall’s villagers are at least getting  a balanced diet now, as he transforms a large patch of the hillside by his base into  carrot farms for more trading options. He even manages to sneak a cactus farm in there, but not  before going to explore more of the Omega Cave, and tracking down a skeleton spawner that’s directly under Stress’s patch of the forest.

**Iskall**: [frantic block breaking] Oh! Hello there!

**Pixlriffs**: After making the spawner usable for bones, he and Stress can farm both types of skeletons, and Iskall cheeses a Wither of his own before heading out to the End in search of his first set of Elytra. The first End-bust goes so smoothly  that he tracks down a bunch more Elytra, then builds a sky island to sell them out  of. It’s a decent sales pitch, considering anyone without an Elytra arrives with the  inconvenience of having to swim up the waterfall, then gets to fly away once they’ve bought one. Let’s just hope he left an ATM up there. Luckily, this won’t be BdoubleO, though what  he had to do for his set of wings is no better. You see, choosing an altruistic route for the season, TangoTek decided to just give  servermates the resources they need. But being TangoTek, he only gives them  at the end of some Jigsaw-eque trap. So to earn his potential five Elytras, Bdubs  has to first prove he can stay afloat for three minutes in a tiny room with lava at the bottom and cacti at the corners. Sure, he’s given a bunch of fireworks to help, but let me tell you, that kinetic energy builds up pretty rapidly.

**BdoubleO100**: At 59, you gave me a stack and 8!

**Tango**: I gave you a- okay! I'm glad you counted, I didn't.

**BdoubleO100**: 😱 ooo! Okay. A stack and 8, I'm at 58!

**Tango**: Okay! [thunk]

**BdoubleO100**:: Ahh man! ["you lost" horn sound]

**Tango**: Oh!! No no!

**BdoubleO100**: MAN!!

**Pixlriffs**: Tango’s deal with Grian and Scar is no less Faustian: as promised, he’s built a raid farmfor the two, far above the villager trading hall they’ve been working on the whole week. But what they forgot in planning is that  the raids will spawn on any block available, so for the farm to truly operate, they will need to dig up all the spawning spots within the perimeter  and flood them all for good measure.

**Tango**: And then I can actually run the  raid farm for the first time. Hopefully it doesn't kill all the villagers  that they've hooked up. That'd be bad!

**Pixlriffs**: In the meantime, GoodTimesWithScar and Grian hold up their side of the deal and keep conditioning the villagers into trading the right stuff at lowered prices. Though, being a tad repetitive, it’s a  project kept mainly off camera in favor of thrilling developments... like surrounding Mumbo’s base with a copper wall for example.

**Grian**: Now, Mumbo is taking  a month off YouTube. So, we fully support Mumbo taking a break,  but does that stop us messing with him, just 'cause he's not here? No. While these blocks weather, the rest of  the bunch Grian moves to The Entity Shop, which steals a water wheel off his house in response.

**Pixlriffs**: After this, it will come as a surprise to no one, hat it’s Grian who starts the Secret April  Fools initiative. Which is like Secret Santa, but with pranks. Which just means, on  this server, it’s like Secret Santa. Not that these people need an extra  reason to prank one another given that someone already waxed the words “sub  Scar” on the walls of the copper box. And someone else already tried  to swap it to “sub Grian”.

**GoodTimeWithScar**: Hmm, we need to think of something else to put here now.

**Pixlriffs**: Scar’s own project for the week is to trade with villagers - we already told you that - but also 
to carve up the rooms within the elven tree of  his into something more resembling a home. Most 
of it comes out more resembling a stairway,  but at least leads to a neat storage room. 
You would think this would help with the  future inevitable forming of a chest monster, but then the room itself is like being swallowed  by a pile of barrels, so, jury’s still out. And while it’s out Scar sentences  this one Wither to not being alive.

**GoodTimeWithScar**: [dramatic music] Come on! Did he die?! Did he die? Where is he, where is he? Did he disappear? [gasps]  Oh no. No. Did he teleport? Can they teleport?!
