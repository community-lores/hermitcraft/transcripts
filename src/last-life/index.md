---
title: 3rd/Last Life
---

The Last Life SMP, formerly known as 3rd Life, is a "hardcore with a twist" survival multiplayer series created by popular Minecraft YouTube content creator [Grian](https://last-life-smp.fandom.com/wiki/Grian). The main hook for the series was that each player would receive three lives upon joining with their final "red" life making them hostile towards other players. Additional rules included only playing for a set amount of time on Fridays, no PVP unless someone on their final life attacked first, proximity chat to be enabled at all times, and in order to better see one another, no helmets were allowed. The players were also confined to a 700 x 700 block area by a world border to keep them in close proximity.[^1]

## Available Transcripts and Playlists

| Participant | Season 1 | Season 2 |
| --- | --- | --- |
| Grian | | |
| GoodTimesWithScar | | |
| Rendog | | |
| Skizzleman | | |
| SmallishBeans | | |
| Smajor1995 | | |
| InTheLittlewood | | |
| ImpluseSv | | |
| Bdouble0100 | | |
| ZombieCleo | | |
| Bigbst4tz2 | | |
| TangoTek | | |
| SolidarityGaming | | |
| EthosLab | | |
| LDShadowLady | N/A | |
| MumboJumbo | N/A | |
| PearlescentMoon | N/A | |


[^1]: Text copied from the Last Life SMP Wiki homepage at <https://last-life-smp.fandom.com/wiki/Last_Life_SMP_Wiki>.