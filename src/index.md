---
title: Homepage
---

!!! warning "Work in progress ahead!"

    Since this project is started recently, this is probably be an work in progress, like site design and even the video trnascripts themselves. Contributions are always welcome!

This website contains human-formatted video transcripts for the Hermitcraft vanilla Minecraft series, among others where the Hermits are involved.

## Who maintains this?