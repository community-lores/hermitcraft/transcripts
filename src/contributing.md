---
title: Contributing to the project
---

## Style Guide

* We follow the [Hermitcraft Wiki's Styyle Guide](https://hermitcraft.fandom.com/wiki/Hermitcraft_Wiki:Policies_and_Guidelines#Manual_of_Style) when writing transripts, which means:
    * Use Hermits' public content creator name instead of their IGN.
    * Use the subject's local dialect when writing/editing transcripts.
        * However, use language intelligible to all varieties wherever it is possible.
        * When quoting, use their preferred national variety of English whether it is the article's variety or not. Do not reformat quotes to be grammatically correct. However, one may leave footnotes on grammatical mistakes explaining how they are incorrect only if they are universally considered mistakes.

## Editing/writing transcripts

We use regular Markdown instead of the usual MediaWiki wikitext here for writing. For stuff like content tags, conslt the theme's reference doc at <https://squidfunk.github.io/mkdocs-material/reference>.
